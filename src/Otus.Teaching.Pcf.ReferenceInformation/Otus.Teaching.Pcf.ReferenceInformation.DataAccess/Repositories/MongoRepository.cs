﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Abstractions;

namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T>  where T: BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoRepository(MongoDbClient client)
        {
            _collection = client.GetCollection<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _collection.Find(_ => true).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _collection.Find(e => ids.Contains(e.Id)).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await _collection.ReplaceOneAsync(x=> x.Id == entity.Id, entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _collection.DeleteOneAsync(x => x.Id == id);
        }
    }
}
