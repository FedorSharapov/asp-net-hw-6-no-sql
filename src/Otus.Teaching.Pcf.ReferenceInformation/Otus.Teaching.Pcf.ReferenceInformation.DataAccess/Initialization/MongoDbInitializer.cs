﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Abstractions;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Data;

namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Initialization
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly IMongoCollection<Preference> _preferences;

        public MongoDbInitializer(MongoDbClient client)
        {
            _preferences = client.GetCollection<Preference>();
        }

        public void Initialize()
        {
            _preferences.DeleteMany(p => true);                         // удаляем все документы 
            _preferences.InsertMany(FakeDataFactory.Preferences);       // добавляем новые документы
        }
    }
}
