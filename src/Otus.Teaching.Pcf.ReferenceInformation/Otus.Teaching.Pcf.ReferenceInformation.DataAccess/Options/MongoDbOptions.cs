﻿namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Options
{
    public class MongoDbOptions
    {
        public string ConnectionString { get; set; }
        public string DbName { get; set; }
    }
}
