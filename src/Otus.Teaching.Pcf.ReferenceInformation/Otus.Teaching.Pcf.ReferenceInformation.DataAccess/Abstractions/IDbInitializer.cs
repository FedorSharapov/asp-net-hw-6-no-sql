﻿namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Abstractions
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
