﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Options;

namespace Otus.Teaching.Pcf.ReferenceInformation.DataAccess
{
    /// <summary>
    /// Клиент для взаимодействия с MongoDb
    /// </summary>
    public class MongoDbClient
    {
        private readonly IMongoDatabase _database;

        public MongoDbClient(MongoDbOptions options)
        {
            var dbClient = new MongoClient(options.ConnectionString);    // новое соединение в пуле соединений
            _database = dbClient.GetDatabase(options.DbName);            // создание БД, если она не создана
        }

        /// <summary>
        /// Получение коллекции. Если она не существует, то будет создана.
        /// </summary>
        /// <typeparam name="T">тип коллекции</typeparam>
        /// <returns>коллекция IMongoCollection<T></returns>
        public IMongoCollection<T> GetCollection<T>() => 
            _database.GetCollection<T>($"{typeof(T).Name}s");
    }
}