using Otus.Teaching.Pcf.ReferenceInformation.Core.Abstractions;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Initialization;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Options;
using Otus.Teaching.Pcf.ReferenceInformation.DataAccess.Repositories;


var builder = WebApplication.CreateBuilder(args);

var mongoDbOpt = builder.Configuration.GetSection(nameof(MongoDbOptions)).Get<MongoDbOptions>();
builder.Services.AddSingleton(mongoDbOpt);
builder.Services.AddScoped<MongoDbClient>();
builder.Services.AddScoped<IRepository<Preference>,MongoRepository<Preference>>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// ������������� ��
new MongoDbInitializer(new MongoDbClient(mongoDbOpt)).Initialize();

app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthorization();

app.MapControllers();

app.Run();
