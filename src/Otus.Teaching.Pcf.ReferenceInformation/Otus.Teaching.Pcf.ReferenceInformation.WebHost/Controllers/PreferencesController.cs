﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Abstractions;
using Otus.Teaching.Pcf.ReferenceInformation.Core.Domain;
using Otus.Teaching.Pcf.ReferenceInformation.WebHost.Models;

namespace Otus.Teaching.Pcf.ReferenceInformation.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _repository;

        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            _repository = preferencesRepository;
        }

        /// <summary>
        /// Получить полный список предпочтений
        /// </summary>
        /// <returns>список предпочтений</returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<List<PreferenceResponse>>> GetAllAsync()
        {
            var preferences = await _repository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns>список предпочтений</returns>
        [HttpGet("[action]")] 
        public async Task<ActionResult<List<PreferenceResponse>>> GetRangeByIdsAsync([FromQuery] List<Guid> ids)
        {
            var preferences = await _repository.GetRangeByIdsAsync(ids);

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить предпочтение
        /// </summary>
        /// <returns>предпочтение</returns>
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<PreferenceResponse>> GetByIdAsync(Guid id)
        {
            var preference = await _repository.GetByIdAsync(id);

            var response = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name
            };

            return Ok(response);
        }

        /// <summary>
        /// Добавить предпочтение
        /// </summary>
        /// <param name="request">предпочтение</param>
        /// <returns>идентификатор</returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> AddAsync(PreferenceRequest request)
        {
            var id = Guid.NewGuid();
            await _repository.AddAsync(new Preference
            {
                Id = id,
                Name = request.Name
            });

            return Ok(id.ToString());
        }


        /// <summary>
        /// Отредактировать предпочтение
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="request">предпочтение</param>
        /// <returns></returns>
        [HttpPut("[action]/{id}")]
        public async Task<ActionResult<Preference>> UpdateAsync(Guid id,PreferenceRequest request)
        {
            await _repository.UpdateAsync(new Preference
            {
                Id = id,
                Name = request.Name
            });

            return NoContent();
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
            return NoContent();
        }
    }
}