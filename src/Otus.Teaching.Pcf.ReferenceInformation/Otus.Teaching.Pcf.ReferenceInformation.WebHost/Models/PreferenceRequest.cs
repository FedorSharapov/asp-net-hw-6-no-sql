﻿namespace Otus.Teaching.Pcf.ReferenceInformation.WebHost.Models
{
    public class PreferenceRequest
    {
        public string Name { get; set; }
    }
}