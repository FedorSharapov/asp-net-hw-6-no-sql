﻿namespace Otus.Teaching.Pcf.ReferenceInformation.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}