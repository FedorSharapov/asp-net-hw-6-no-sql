﻿using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.ReferenceInformation.Core.Domain
{
    public class BaseEntity
    {
        [BsonId]
        public Guid Id { get; set; }
    }
}
