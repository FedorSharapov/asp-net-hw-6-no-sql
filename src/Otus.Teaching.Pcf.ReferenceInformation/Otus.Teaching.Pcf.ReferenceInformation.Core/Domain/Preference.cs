﻿namespace Otus.Teaching.Pcf.ReferenceInformation.Core.Domain
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}