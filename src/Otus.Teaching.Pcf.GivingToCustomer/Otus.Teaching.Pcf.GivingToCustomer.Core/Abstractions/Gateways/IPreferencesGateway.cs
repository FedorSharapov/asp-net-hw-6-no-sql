﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IPreferencesGateway
    {
        Task<List<Preference>> GetAllAsync();
        Task<List<Preference>> GetRangeByIdsAsync(List<Guid> preferenceIds);
        Task<Preference> GetByIdAsync(Guid id);
    }
}