﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Fakes
{
    public class PreferencesGateway : IPreferencesGateway
    {
        public async Task<List<Preference>> GetAllAsync()
        {
            return TestDataFactory.Preferences;
        }
        public async Task<List<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return TestDataFactory.Preferences.Where(p => ids.Contains(p.Id)).ToList();
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            return TestDataFactory.Preferences.First(p => p.Id == id);
        }
    }
}