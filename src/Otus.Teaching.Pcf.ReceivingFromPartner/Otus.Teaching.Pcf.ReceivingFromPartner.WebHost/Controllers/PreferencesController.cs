﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IPreferencesGateway _preferencesGateway;
        private readonly ICacheService _cacheService;

        public PreferencesController(IPreferencesGateway preferencesGateway, ICacheService cacheService)
        {
            _preferencesGateway = preferencesGateway;
            _cacheService = cacheService;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _cacheService
                .SetKey("GetPreferencesAsync")
                .SetOptions(new DistributedCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(30))
                    .SetAbsoluteExpiration(DateTime.Now.AddMinutes(1)))
                .GetAsync(_preferencesGateway.GetAllAsync);

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
            /*
            // Вариант 2
            var cacheKey = "GetPreferencesAsync";

            // запрашиваем данные из кэш
            var preferences = await _cacheService.GetCacheAsync<List<Preference>>(cacheKey);
            if (preferences == null)
            {
                // запрашиваем данные из микросервиса ReferenceInformation
                preferences = await _preferencesGateway.GetAllAsync();

                // добавляем данные в кэш
                await _cacheService.SetCacheAsync(cacheKey, preferences, new DistributedCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(5))  
                    .SetAbsoluteExpiration(DateTime.Now.AddHours(6)));
            }

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);*/
        }

        /// <summary>
        /// Получить предпочтение
        /// </summary>
        /// <returns>предпочтение</returns>
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<PreferenceResponse>> GetByIdAsync(Guid id)
        {
            var preference = await _cacheService
                .SetKey(id.ToString())
                .GetAsync(_preferencesGateway.GetByIdAsync,id);

            var response = new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name
            };

            return Ok(response);
        }
    }
}