﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions;
using static Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions.ICacheService;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services
{
    public class CacheService : ICacheService
    {
        private readonly IDistributedCache _distributedCache;

        private string _key;
        private DistributedCacheEntryOptions _options;

        public ICacheService SetKey(string key)
        {
            _key = key;
            return this;
        }
        public ICacheService SetOptions(DistributedCacheEntryOptions options)
        {
            _options = options;
            return this;
        }

        public CacheService(IDistributedCache distributedCache) 
        {
            _distributedCache = distributedCache;
        }

        public async Task<TResult> GetCacheAsync<TResult>(string key)
        {
            // запрашиваем данные из кэш
            var valueCache = await _distributedCache.GetAsync(key);
            if (valueCache != null)
            {
                // преобразовываем данные
                var valueJson = Encoding.UTF8.GetString(valueCache);
                return JsonSerializer.Deserialize<TResult>(valueJson);
            }

            return default;
        }

        public async Task SetCacheAsync<TCache>(string key, TCache obj, DistributedCacheEntryOptions options = null)
        {
            // преобразовываем данные
            var valueCache = JsonSerializer.SerializeToUtf8Bytes(obj);

            options ??= new DistributedCacheEntryOptions()
                // срок действия записи истечет, если к ней не обращались в течение установленного периода времени
                .SetSlidingExpiration(TimeSpan.FromSeconds(30))
                // срок действия записи истечет через заданный период времени не зависимо от SetSlidingExpiration
                .SetAbsoluteExpiration(DateTime.Now.AddMinutes(5));

            // записываем данные в кэш
            await _distributedCache.SetAsync(key, valueCache, options);
        }

        public async Task<TResult> GetAsync<TResult>(GetData<TResult> getData) 
            => await GetAsync(_key, getData, _options);
        
        public async Task<TResult> GetAsync<TResult>(string key, GetData<TResult> getData, DistributedCacheEntryOptions options = null)
        {
            // запрашиваем данные из кэш
            var result = await GetCacheAsync<TResult>(_key);
            if (result == null)
            {
                // запрашиваем данные из источника
                result = await getData();
                // добавляем в кэш
                await SetCacheAsync(key, result, options);
            }

            return result;
        }

        public async Task<TResult> GetAsync<T, TResult>(GetData<T, TResult> getData, T item) 
            => await GetAsync(_key, getData, item, _options);

        public async Task<TResult> GetAsync<T, TResult>(string key, GetData<T, TResult> getData, T item, DistributedCacheEntryOptions options = null)
        {
            // запрашиваем данные из кэш
            var result = await GetCacheAsync<TResult>(_key);
            if (result == null)
            {
                // запрашиваем данные из источника
                result = await getData(item);
                // добавляем в кэш
                await SetCacheAsync(key, result, options);
            }

            return result;
        }
    }
}
