﻿using Microsoft.Extensions.Caching.Distributed;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Services.Abstractions
{
    public interface ICacheService
    {
        /// <summary>
        /// Делегат получения данных из источника
        /// </summary>
        /// <typeparam name="T">Тип входного параметра</typeparam>
        /// <typeparam name="TResult">Тип возвращаемого значения(й)</typeparam>
        /// <param name="item">Входной параметр</param>
        /// <returns></returns>
        delegate Task<TResult> GetData<T, TResult>(T item);
        /// <inheritdoc cref="GetData{T, TResult}"/>
        delegate Task<TResult> GetData<TResult>();

        /// <summary>
        /// Задать ключ значение
        /// </summary>
        /// <param name="key">Ключ-значение</param>
        /// <returns>текущий экземпляр ICacheService</returns>
        ICacheService SetKey(string key);

        /// <summary>
        /// Задать опции кэширования
        /// </summary>
        /// <param name="options">Опции кэширования</param>
        /// <returns>текущий экземпляр ICacheService</returns>
        ICacheService SetOptions(DistributedCacheEntryOptions options);

        /// <summary>
        /// Получить данные из кэш
        /// </summary>
        /// <typeparam name="TResult">Тип возвращаемого значения(й)</typeparam>
        /// <param name="key">Ключ-значение</param>
        /// <returns>Данные из кэш типа TResult</returns>
        Task<TResult> GetCacheAsync<TResult>(string key = null);

        /// <summary>
        /// Записать данные в кэш
        /// </summary>
        /// <typeparam name="TCache">Тип кэшируемого объекта(ов)</typeparam>
        /// <param name="key">Ключ-значение</param>
        /// <param name="obj">Кэшируемый объект(ы)</param>
        /// <param name="options">Опции кэширования</param>
        /// <returns></returns>
        Task SetCacheAsync<TCache>(string key, TCache obj, DistributedCacheEntryOptions options = null);

        /// <summary>
        /// Получить данные из кэш. Если данные отсутствуют, 
        /// то будет вызван метод получения данных, после чего записаны данные в кэш.
        /// </summary>
        /// <typeparam name="T">Тип входного параметра метода получения данных из источника GetData</typeparam>
        /// <typeparam name="TResult">Тип возвращаемого значения(й)</typeparam>
        /// <param name="key">Ключ-значение</param>
        /// <param name="options">Опции кэширования</param>
        /// <param name="getData">Метод получения данных из источника</param>
        /// <param name="item">Входной параметр метода получения данных из источника GetData</param>
        /// <returns>Запрашиваемые данные типа TResult из кэша или из источника данных</returns>
        Task<TResult> GetAsync<T, TResult>(string key, GetData<T, TResult> getData, T item, DistributedCacheEntryOptions options = null);
        /// <inheritdoc cref="GetAsync"/>
        Task<TResult> GetAsync<T, TResult>(GetData<T, TResult> getData, T item);
        /// <inheritdoc cref="GetAsync"/>
        Task<TResult> GetAsync<TResult>(string key, GetData<TResult> getData, DistributedCacheEntryOptions options = null);
        /// <inheritdoc cref="GetAsync"/>
        Task<TResult> GetAsync<TResult>(GetData<TResult> getData);
    }
}
