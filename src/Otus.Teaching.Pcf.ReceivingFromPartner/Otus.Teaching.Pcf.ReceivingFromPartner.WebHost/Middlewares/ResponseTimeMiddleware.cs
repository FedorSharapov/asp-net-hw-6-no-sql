﻿using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Middlewares
{
    public class ResponseTimeMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseTimeMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
             
            context.Response.OnStarting(() => {
                stopWatch.Stop();
                context.Response.Headers.Add("X-Response-Time-ms", stopWatch.ElapsedMilliseconds.ToString());
                return Task.CompletedTask;
            });

            await _next(context);
        }
    }
}
