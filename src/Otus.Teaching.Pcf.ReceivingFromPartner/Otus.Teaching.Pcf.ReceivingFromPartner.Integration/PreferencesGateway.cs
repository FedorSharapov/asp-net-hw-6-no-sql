﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class PreferencesGateway : IPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public PreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri(_httpClient.BaseAddress.OriginalString + "api/v1/Preferences/");
        }

        public async Task<List<Preference>> GetAllAsync()
        {
            var response = await _httpClient.GetAsync($"GetAll");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error request: {response.StatusCode}");

            var jsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Preference>>(jsonString);
        }

        public async Task<List<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var query = $"?ids={string.Join("&ids=", ids)}";
            var response = await _httpClient.GetAsync($"GetRangeByIds{query}");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error request: {response.StatusCode}");

            var jsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Preference>>(jsonString);
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            var response = await _httpClient.GetAsync($"GetById/{id}");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error request: {response.StatusCode}");

            var jsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Preference>(jsonString);
        }
    }
}